#!/usr/bin/env bash
rm function.zip
cd package/
zip -r9 ${OLDPWD}/function.zip .
cd ..
zip -g function.zip main.py